# Check for libusb version >= [$1]
# Keith Clayton <keith@claytons.org>, 2001.

AC_DEFUN(AC_LIB_USB_VERSION,
  [ AC_MSG_CHECKING(libusb version >= [$1])
    LIBUSB_VERSION=$($LIBUSBCONFIG --version)
    LIBUSB_MAJOR=`$LIBUSBCONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/' | cut -b 1`
    LIBUSB_MINOR=`$LIBUSBCONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/' | cut -b 1`
    LIBUSB_MICRO=`$LIBUSBCONFIG --version | sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if [[ "$LIBUSB_MINOR" -lt "$(echo "$1" | cut -b 3)" ]] ; then
        AC_MSG_RESULT(no)
        AC_MSG_WARN(You have version $LIBUSB_MAJOR.$LIBUSB_MINOR.$LIBUSB_MINOR)
        exit -1
    else
        LIBUSB_LETTER=`echo $LIBUSB_MICRO | cut -b 2`
        LIBUSB_DIGIT=`echo $LIBUSB_MICRO | cut -b 1`
        if [[ "$LIBUSB_DIGIT" -lt "$(echo "$1" | cut -b 5)"  ]] ; then
                AC_MSG_RESULT(no)
                AC_MSG_ERROR(You have version $LIBUSB_MAJOR.$LIBUSB_MINOR.$LIBUSB_MICRO)
        fi
        AC_MSG_RESULT($LIBUSB_VERSION)
    fi
    ])

