Fonts -- (c) 1998 - 2000 'ck!' [Freaky Fonts]

These fonts are freeware, for personal, non-commercial use!
These font files may not be modified and this readme file must be 
included with each font.

This zip includes Windows bitmap-fonts (*.fon).

If you like the fonts, please e-mail me.

Visit -+ Freaky Fonts +- for updates and new fonts (PC & MAC):

http://come.to/freakyfonts
http://www.geocities.com/Area51/Shadowlands/7677/

Comments, bugs, suggestions? e-mail: 
ckrule@geocities.com


 