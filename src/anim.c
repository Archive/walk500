

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "callbacks.h"
#include "rio500.h"

extern GtkWidget *mainwindow;
extern gint song_selected;
extern gint memtype;		/* 0 for internal, 1 for flash */
extern gint total_mem;
extern GladeXML *xml;

void walk_dialogbmp_cancel(void)
{
	GtkWidget *animbmp;

        animbmp = glade_xml_get_widget(xml, "dialogbmp");
        gtk_widget_hide(animbmp);
}

void walk_dialogbmp_ckinput(GtkWidget *entry)
{
	GtkWidget *button;
	struct stat stats;
	gchar *path;

	button = glade_xml_get_widget(xml, "animbmp_ok_button");
	path = gtk_entry_get_text(GTK_ENTRY(entry));
	if(path && 
			!stat(path, &stats) &&
			S_ISREG(stats.st_mode) &&
			stats.st_size > 0)
	{
		gtk_widget_set_sensitive(button, TRUE);
	} else {
		gtk_widget_set_sensitive(button, FALSE);
	}
}

void walk_dialogbmp_apply(void)
{
	GtkWidget *widget;
	gchar *input, *output, *comment;
	gint delay, final_delay, loop_count, clear_lcd;

	/* input */
	widget = glade_xml_get_widget(xml, "input_entry");
	input = gtk_entry_get_text(GTK_ENTRY(widget));

	/* output */
	widget = glade_xml_get_widget(xml, "output_entry");
	output = gtk_entry_get_text(GTK_ENTRY(widget));

	/* comment */
	widget = glade_xml_get_widget(xml, "entry6");
	comment = gtk_entry_get_text(GTK_ENTRY(widget));

	/* delay */
	widget = glade_xml_get_widget(xml, "pause_check");
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) == TRUE)
	{
		widget = glade_xml_get_widget(xml, "pause_spinbutton");
		delay = gtk_spin_button_get_value_as_int
			(GTK_SPIN_BUTTON(widget));;
	} else {
		delay = 1;
	}

	/* final_delay and loop_count */
	widget = glade_xml_get_widget(xml, "last_pause_toggle");
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) == TRUE)
	{
		widget = glade_xml_get_widget(xml, "spinbutton8");
		final_delay = gtk_spin_button_get_value_as_int
			(GTK_SPIN_BUTTON(widget));
		loop_count = 0;
	} else {
		widget = glade_xml_get_widget(xml, "spinbutton9");
		final_delay = 0;
		loop_count = gtk_spin_button_get_value_as_int
			(GTK_SPIN_BUTTON(widget));
	}

	/* clear_lcd */
	widget = glade_xml_get_widget(xml, "checkbutton5");
	clear_lcd = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));;

	E(rio_bmp2rioani(delay, final_delay, loop_count, clear_lcd,
				comment, input, output));
}

void walk_dialogani_cancel(void)
{
        GtkWidget *ani;

        ani = glade_xml_get_widget(xml, "dialoganim");
        gtk_widget_hide(ani);
}


void walk_dialogani_ckinput(GtkWidget *entry)
{
	GtkWidget *button, *label;
	struct stat stats;
	gchar *path, *comment;

	label = glade_xml_get_widget(xml, "label69");
	button = glade_xml_get_widget(xml, "button4");
	path = gtk_entry_get_text(GTK_ENTRY(entry));
	if(path && 
			!stat(path, &stats) &&
			S_ISREG(stats.st_mode) &&
			stats.st_size > 0)
	{
		comment = rio_anim_comment(path);
		gtk_widget_set_sensitive(button, TRUE);
		gtk_label_set_text(GTK_LABEL(label),
				g_strdup(comment));
	} else {
		gtk_widget_set_sensitive(button, FALSE);
		gtk_label_set_text(GTK_LABEL(label),
				_("Not an animation"));
	}
}

void walk_dialogani_apply(void)
{
	GtkWidget *entry;
	gchar *filename;

	walk_dialogani_cancel();
	entry = glade_xml_get_widget(xml, "entry5");
	filename = gtk_entry_get_text(GTK_ENTRY(entry));
	rio_set_anim(rio_hw, filename);
}

void walk_ani_remove(void)
{
	rio_remove_anim(rio_hw);
}

