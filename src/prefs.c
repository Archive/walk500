

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "callbacks.h"
#include "rio500.h"

extern GtkWidget *mainwindow;
extern gint song_selected;
extern gint memtype;		/* 0 for internal, 1 for flash */
extern gint total_mem;
extern GladeXML *xml;

/* Configuration values */
gboolean confirm_format;
gboolean confirm_del;
gboolean confirm_del_folder;
gboolean confirm_del_empty_folder;
gchar *font_path;

void walk_prefs_apply(gint page_num)
{
	GtkWidget *prefs;

	prefs = glade_xml_get_widget(xml, "prefs");

	/* Applying preferences goes here */
	switch (page_num) {
	case 0:
		{
			GtkWidget *toggle;

			toggle = glade_xml_get_widget(xml,
					  "checkbutton1");
			confirm_format =
			    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON
							 (toggle));

			toggle = glade_xml_get_widget(xml,
					  "checkbutton2");
			confirm_del =
			    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON
							 (toggle));

			toggle = glade_xml_get_widget(xml,
					  "checkbutton3");
			confirm_del_folder =
			    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON
							 (toggle));

			toggle = glade_xml_get_widget(xml,
					  "checkbutton4");
			confirm_del_empty_folder =
			    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON
							 (toggle));
		}
		break;
	case 1:
		{
			/* If the Rio is uploading, dismiss the changes */
			/* otherwise apply in here */
		}
	}

	walk_save_config();
}


void walk_load_config(void)
{
	gnome_config_push_prefix("/walk500/General/");

	confirm_format = gnome_config_get_bool("format=true");
	confirm_del = gnome_config_get_bool("del=true");
	confirm_del_folder = gnome_config_get_bool("del_folder=true");
	confirm_del_empty_folder = gnome_config_get_bool
	    ("del_empty_folder=false");
	font_path = gnome_config_get_string("font_path=DEFAULT_FONT_PATH");

	gnome_config_pop_prefix();
}

void walk_save_config(void)
{
	gnome_config_push_prefix("/walk500/General/");

	gnome_config_set_bool("format", confirm_format);
	gnome_config_set_bool("del", confirm_del);
	gnome_config_set_bool("del_folder", confirm_del_folder);
	gnome_config_set_bool("del_empty_folder",
			      confirm_del_empty_folder);
	gnome_config_set_string("font_path", font_path);

	gnome_config_sync();
	gnome_config_pop_prefix();
}

void walk_prefs_activate(void)
{
	GtkWidget *prefs, *tmp_widget;

	prefs = glade_xml_get_widget(xml, "prefs");
	{
		GtkWidget *mainwindow;

		mainwindow = glade_xml_get_widget(xml, "mainwindow");
		gnome_dialog_set_parent(GNOME_DIALOG(prefs),
				GTK_WINDOW(mainwindow));
	}
	/* We activate the preferences widgets before they are shown 
	 * and connect the signals*/

	tmp_widget = glade_xml_get_widget(xml, "checkbutton1");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tmp_widget),
				     confirm_format);
	tmp_widget = glade_xml_get_widget(xml, "checkbutton2");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tmp_widget),
				     confirm_del);
	tmp_widget = glade_xml_get_widget(xml, "checkbutton3");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tmp_widget),
				     confirm_del_folder);
	tmp_widget = glade_xml_get_widget(xml, "checkbutton4");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tmp_widget),
				     confirm_del_empty_folder);

	gtk_widget_show(prefs);
}

