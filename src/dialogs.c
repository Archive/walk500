

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "callbacks.h"
#include "rio500.h"

extern GtkWidget *mainwindow;
extern gint song_selected, folder_selected;
extern gint total_mem;
extern GList *int_content, *ext_content;
extern GladeXML *xml;

void walk_add_song_dialog(void)
{
	GtkWidget *file_select;

	file_select = gtk_file_selection_new(_("Please select a Song"));

	gtk_signal_connect(GTK_OBJECT
			   (GTK_FILE_SELECTION(file_select)->ok_button),
			   "clicked", GTK_SIGNAL_FUNC(walk_add_song),
			   NULL);
	gtk_object_set_data(GTK_OBJECT
			    (GTK_FILE_SELECTION(file_select)->ok_button),
			    "file_select", file_select);
	gtk_signal_connect_object(GTK_OBJECT
				  (GTK_FILE_SELECTION
				   (file_select)->ok_button), "clicked",
				  GTK_SIGNAL_FUNC(gtk_widget_destroy),
				  (gpointer) file_select);
	gtk_signal_connect_object(GTK_OBJECT
				  (GTK_FILE_SELECTION
				   (file_select)->cancel_button),
				  "clicked",
				  GTK_SIGNAL_FUNC(gtk_widget_destroy),
				  (gpointer) file_select);

	gtk_widget_show(file_select);
}

void walk_add_folder(void)
{
	GtkWidget *folder_dialog;
	GtkWidget *entry;
	gint reply;

	folder_dialog = gnome_message_box_new
	    (_("Enter the name of the new folder"),
	     GNOME_MESSAGE_BOX_QUESTION,
	     GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);

	gnome_dialog_set_close(GNOME_DIALOG(folder_dialog), TRUE);
	gnome_dialog_close_hides(GNOME_DIALOG(folder_dialog), TRUE);
	gnome_dialog_set_parent(GNOME_DIALOG(folder_dialog),
				GTK_WINDOW(mainwindow));
	gnome_dialog_set_default(GNOME_DIALOG(folder_dialog), GNOME_OK);

	entry = gtk_entry_new();
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(folder_dialog)->vbox),
			  entry);
	gnome_dialog_editable_enters(GNOME_DIALOG(folder_dialog),
				     GTK_EDITABLE(entry));
	gtk_widget_grab_focus(GTK_WIDGET(GTK_EDITABLE(entry)));

	gtk_widget_show_all(folder_dialog);

	reply = gnome_dialog_run(GNOME_DIALOG(folder_dialog));

	if (reply == GNOME_YES) {
		gchar *folder_name;

		folder_name = gtk_entry_get_text(GTK_ENTRY(entry));
		walk_rio_in_use(TRUE);
		walk_which_mem();	/* Select the right memory type */
		E(rio_add_folder(rio_hw, folder_name));
		walk_rescan();
		walk_rio_in_use(FALSE);
	}
	gtk_widget_destroy(folder_dialog);
}

void walk_delete_folder(void)
{
	RioFolderEntry *folder_entry;
	gchar *tmp=NULL;
	gint memtype;
	gint folder_num;

	GtkWidget *sure_dialog;
	gint reply;
	gboolean delete_folder_ask = FALSE;

	walk_rio_in_use(TRUE);

	memtype = walk_which_mem();
	folder_num = walk_which_folder();

	if (memtype == 0) {
		folder_entry = (RioFolderEntry *)
		    (g_list_nth_data(int_content, folder_num));
	} else {
		folder_entry = (RioFolderEntry *)
		    (g_list_nth_data(ext_content, folder_num));
	}

	/* Mangling to see if we should ask for deletion of this folder */
	if (folder_entry->songs == NULL) {
		if (confirm_del_empty_folder) {
			delete_folder_ask = TRUE;
		}
	} else {
		if (confirm_del_folder) {
			delete_folder_ask = TRUE;
		}
	}

	if (delete_folder_ask) {
		GtkWidget *clist;
		gchar *msg;

		clist = glade_xml_get_widget(xml, "clist2");
		tmp = gtk_clist_get_row_data(GTK_CLIST(clist),
					     folder_selected);

		msg = g_strdup_printf(_("Are you sure you want to delete folder %s ?"), tmp);
		sure_dialog =
		    gnome_message_box_new(msg, GNOME_MESSAGE_BOX_WARNING,
				GNOME_STOCK_BUTTON_YES,
				GNOME_STOCK_BUTTON_NO, NULL);
		gtk_widget_show(sure_dialog);

		reply = gnome_dialog_run(GNOME_DIALOG(sure_dialog));
		g_free(msg);
	} else {
		reply = GNOME_YES;
	}

	if (reply == GNOME_YES) {
		E(rio_del_folder(rio_hw, folder_num));
		walk_rescan();
	}
	walk_rio_in_use(FALSE);
}

void walk_delete_song(void)
{
	GtkWidget *delete_dialog, *clist;
	gint folder_num;
	gchar *tmp;
	gint reply;

	walk_rio_in_use(TRUE);

	walk_which_mem();
	folder_num = walk_which_folder();

	if (confirm_del) {
		gchar *msg;

		clist = glade_xml_get_widget(xml, "clist1");
		tmp =
		    gtk_clist_get_row_data(GTK_CLIST(clist),
					   song_selected);

		msg = g_strdup_printf(_("Do really want to delete song %s ?"),
				tmp);

		delete_dialog =
		    gnome_message_box_new(msg , GNOME_MESSAGE_BOX_WARNING,
					  GNOME_STOCK_BUTTON_YES,
					  GNOME_STOCK_BUTTON_NO, NULL);
		gnome_dialog_set_parent(GNOME_DIALOG(delete_dialog),
					GTK_WINDOW(mainwindow));
		gnome_dialog_set_default(GNOME_DIALOG(delete_dialog),
					 GNOME_YES);
		gtk_widget_show(delete_dialog);

		reply = gnome_dialog_run(GNOME_DIALOG(delete_dialog));
		g_free(msg);
	} else {
		reply = GNOME_YES;
	}

	if (reply == GNOME_YES) {
		E(rio_del_song(rio_hw, folder_num, song_selected));
		walk_rescan();
	}
	walk_rio_in_use(FALSE);
}

void walk_format(void)
{
	GtkWidget *format_dialog;
	gchar *tmp = NULL;
	gint reply;

	walk_rio_in_use(TRUE);
	walk_which_mem();

	if (confirm_format) {
		gint card;

		card = walk_which_mem();
		if (card == 0) {
			tmp = g_strdup(_("Do you really want to format the Internal RAM ?"));
		} else {
			tmp = g_strdup(_("Do you really want to format the Flash Card ?"));
		}

		format_dialog =
		    gnome_message_box_new(tmp,
			GNOME_MESSAGE_BOX_WARNING, GNOME_STOCK_BUTTON_YES,
			GNOME_STOCK_BUTTON_NO, NULL);
		gnome_dialog_set_parent(GNOME_DIALOG(format_dialog),
					GTK_WINDOW(mainwindow));
		gnome_dialog_set_default(GNOME_DIALOG(format_dialog),
					 GNOME_NO);
		gtk_widget_show(format_dialog);

		reply = gnome_dialog_run(GNOME_DIALOG(format_dialog));
	} else {
		reply = GNOME_YES;
	}

	if (reply == GNOME_YES) {
		walk_long_op();
		E(rio_format(rio_hw));
		walk_rescan();
	}
	if (tmp != NULL)
		g_free(tmp);
	walk_rio_in_use(FALSE);
}

void walk_init_fail_dialog(void)
{
	GtkWidget *fail_dialog;
	gint reply;

	fail_dialog =
	    gnome_message_box_new(_
				  ("Rio initialization failed\nDo you want to retry connecting to the Rio ?"),
				  GNOME_MESSAGE_BOX_ERROR,
				  GNOME_STOCK_BUTTON_YES,
				  GNOME_STOCK_BUTTON_NO, NULL);
	gnome_dialog_set_parent(GNOME_DIALOG(fail_dialog),
				GTK_WINDOW(mainwindow));
	gnome_dialog_set_default(GNOME_DIALOG(fail_dialog), GNOME_YES);
	gtk_widget_show(fail_dialog);

	reply = gnome_dialog_run(GNOME_DIALOG(fail_dialog));
	switch (reply) {
	case GNOME_YES:
		return;
		break;
	default:
		exit(0);
		break;
	}
}

void walk_io_error(gint error)
{
	GtkWidget *error_dialog;
	gchar *msg= NULL;

	/* set the right error message 
	 * RIO_SUCCESS: we don't need this, it's succeeded !
	 */ 
	switch (error)
	{
	case PC_MEMERR:
		msg = g_strdup(_("Host memory allocation failed!"));
		break;
	case RIO_NOMEM:
		msg = g_strdup(_("Not enough memory left on device!"));
		break;
	case RIO_NODIR:
		msg = g_strdup(_("No such directory!"));
		break;
	case RIO_INITCOMM:
		msg = g_strdup(_("Unable to initiate\ncommunication with device."));
		break;
	case RIO_ENDCOMM:
		msg = g_strdup(_("Error communicating with device."));
		break;
	case RIO_FORMAT:
		msg = g_strdup(_("Error formatting device."));
		break;
	case RIO_FILEERR:
		msg = g_strdup(_("File IO error"));
		break;
	case RIO_FIRM_1X:
		msg = g_strdup(_("Animation larger than 48k for 1.x firmware."));
		break;
	case RIO_FIRM_2X:
		msg = g_strdup(_("Animation larger than 64k for 2.x firmware."));
		break;
	case RIO_NOTANIM:
		msg = g_strdup(_("File isn't an animation file or not a valid file."));
		break;
	case RIO_BMP2ANI:
		msg = g_strdup(_("Error while converting BMP file to animation."));
		break;
	case WALK500_GLADE:
		msg = g_strdup(_("Couldn't load the Glade file.\nMake sure that Walk500 is properly installed."));
		break;
	default:
		msg = g_strdup_printf(_("Unknown error: %d"), error);
		break;
	}

	error_dialog =
		gnome_message_box_new(msg,
				GNOME_MESSAGE_BOX_ERROR,
				GNOME_STOCK_BUTTON_OK, NULL);
	if (mainwindow != NULL)
		gnome_dialog_set_parent(GNOME_DIALOG(error_dialog),
				GTK_WINDOW(mainwindow));
	gnome_dialog_set_default(GNOME_DIALOG(error_dialog), GNOME_YES);

	gtk_widget_show(error_dialog);
	gnome_dialog_run(GNOME_DIALOG(error_dialog));
	g_free(msg);
}

void walk_rename_folder(void)
{
	GtkWidget *folder_dialog;
	GtkWidget *entry;
	gchar *folder_name = NULL;
	gint reply;
	gchar *text;

	folder_dialog = gnome_message_box_new
		(_("Enter the new name for the folder"),
		 GNOME_MESSAGE_BOX_QUESTION,
		 GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);

	gnome_dialog_set_close(GNOME_DIALOG(folder_dialog), TRUE);
	gnome_dialog_close_hides(GNOME_DIALOG(folder_dialog), TRUE);
	gnome_dialog_set_parent(GNOME_DIALOG(folder_dialog),
			GTK_WINDOW(mainwindow));
	gnome_dialog_set_default(GNOME_DIALOG(folder_dialog), GNOME_OK);

	entry = gtk_entry_new();
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(folder_dialog)->vbox),
			entry);
	gnome_dialog_editable_enters(GNOME_DIALOG(folder_dialog),
			GTK_EDITABLE(entry));
	gtk_widget_grab_focus(GTK_WIDGET(GTK_EDITABLE(entry)));

	/* Set the old folder name as the default */
	{
		GtkWidget *clist;
		
		clist = glade_xml_get_widget(xml, "clist2");
		text = gtk_clist_get_row_data(GTK_CLIST(clist),
				folder_selected);
	}

	gtk_entry_set_text((GtkEntry *)(entry), text);
	gtk_editable_select_region(GTK_EDITABLE(entry), 0, -1);

	gtk_widget_show_all(folder_dialog);

	reply = gnome_dialog_run(GNOME_DIALOG(folder_dialog));

	if (reply == GNOME_YES) {
		gint folder;

		folder_name = gtk_entry_get_text(GTK_ENTRY(entry));
		/* If the old and the new name are the same finish */
		if (strcmp(folder_name, text) != 0)
		{
			walk_rio_in_use(TRUE);
			/* Select the right memory type */
			walk_which_mem();
			folder = walk_which_folder();
			rio_rename_folder(rio_hw, folder, folder_name);
			walk_rescan();
			walk_rio_in_use(FALSE);
		}
	}
	gtk_widget_destroy(folder_dialog);
}

void walk_rename_song(void)
{
	GtkWidget *song_dialog;
	GtkWidget *entry;
	gint reply;
	gchar *text;

	song_dialog = gnome_message_box_new
		(_("Enter the new name for the song"),
		 GNOME_MESSAGE_BOX_QUESTION,
		 GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);

	gnome_dialog_set_close(GNOME_DIALOG(song_dialog), TRUE); 
	gnome_dialog_close_hides(GNOME_DIALOG(song_dialog), TRUE);
	gnome_dialog_set_parent(GNOME_DIALOG(song_dialog),
			GTK_WINDOW(mainwindow));
	gnome_dialog_set_default(GNOME_DIALOG(song_dialog), GNOME_OK);

	entry = gtk_entry_new();
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(song_dialog)->vbox),
			entry);
	gnome_dialog_editable_enters(GNOME_DIALOG(song_dialog),
			GTK_EDITABLE(entry));
	gtk_widget_grab_focus(GTK_WIDGET(GTK_EDITABLE(entry)));
	/* Set the old song name as the default */
	{
		GtkWidget *clist;

		clist = glade_xml_get_widget(xml, "clist1");
		text = gtk_clist_get_row_data(GTK_CLIST(clist),
				song_selected);
	}

	gtk_entry_set_text((GtkEntry *)(entry), text);
	gtk_editable_select_region(GTK_EDITABLE(entry), 0, -1);
	gtk_widget_show_all(song_dialog);

	reply = gnome_dialog_run(GNOME_DIALOG(song_dialog));

	if (reply == GNOME_YES) {
		gchar *song_name;
		gint folder;

		song_name = gtk_entry_get_text(GTK_ENTRY(entry));
		/* If the old and the new name are the same finish */
		if (strcmp(song_name, text) != 0)
		{
			walk_rio_in_use(TRUE);
			/* Select the right memory type */
			walk_which_mem();
			folder = walk_which_folder();
			rio_rename_song(rio_hw, folder, song_selected,
					song_name);
			walk_rescan();
			walk_rio_in_use(FALSE);
		}
		g_free(song_name);
	}
	gtk_widget_destroy(song_dialog);
}

void walk_get_song_dialog(void)
{
	GtkWidget *file_select;
	GtkWidget *clist;

	file_select = gtk_file_selection_new(_("Please select a destination"));
	clist = glade_xml_get_widget(xml, "clist1");
	gtk_file_selection_set_filename((GtkFileSelection *)file_select,
		 gtk_clist_get_row_data(GTK_CLIST(clist), song_selected));

	gtk_signal_connect(GTK_OBJECT
			(GTK_FILE_SELECTION(file_select)->ok_button),
			"clicked", GTK_SIGNAL_FUNC(walk_get_song),
			NULL);
	gtk_object_set_data(GTK_OBJECT
			(GTK_FILE_SELECTION(file_select)->ok_button),
			"file_select", file_select);
	gtk_signal_connect_object(GTK_OBJECT
			(GTK_FILE_SELECTION
			 (file_select)->cancel_button),
			"clicked",
			GTK_SIGNAL_FUNC(gtk_widget_destroy),
			(gpointer) file_select);

	gtk_widget_show(file_select);
}

