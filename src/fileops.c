
#include <gnome.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>

#include "callbacks.h"
#include "rio500.h"

#define PATH_MAX_LENGTH 1024

extern gint song_selected, folder_selected;
extern GList *tree_list;
extern GList *int_content, *ext_content;
extern GtkWidget *mainwindow;
extern GladeXML *xml;

void walk_add_song(GtkWidget * widget)
{
        GtkWidget *file_select;
        gchar *filename;
        GList *filelist = NULL;

        walk_rio_in_use(TRUE);
        file_select =
            gtk_object_get_data(GTK_OBJECT(widget), "file_select");
        gtk_widget_hide(file_select);
        filename =
            gtk_file_selection_get_filename(GTK_FILE_SELECTION
                                            (file_select));
        /* First element always NULL, so we discard it */
        filelist = g_list_append(filelist, g_strdup(filename));
	gtk_widget_destroy(file_select);
        walk_upload(filelist);
        walk_rio_in_use(FALSE);
}

void walk_get_song(GtkWidget * widget)
{
	GtkWidget *file_select;
	gchar *filename;
	gint folder;

	walk_rio_in_use(TRUE);
	folder = walk_which_folder();
	file_select =
		gtk_object_get_data(GTK_OBJECT(widget), "file_select");
	gtk_widget_hide(file_select);
	filename =
		g_strdup (gtk_file_selection_get_filename(GTK_FILE_SELECTION
				(file_select)));
	gtk_widget_destroy(file_select);
	E(rio_get_song(rio_hw, filename, folder, song_selected));
	walk_rio_in_use(FALSE);
	g_free (filename);
}

void walk_drop_onpix(GtkSelectionData * data)
{
	GList *content;
	FolderInfo *info;
	GtkWidget *tree;

	info = g_list_nth_data(tree_list, folder_selected);

	if (info->memtype == 0) {
		content = int_content;
	} else {
		content = ext_content;
	}

	if (!content)
	{
		walk_rio_in_use(TRUE);
		walk_which_mem();
		E(rio_add_folder(rio_hw, "songs"));
		walk_rescan();
	}

	tree = glade_xml_get_widget(xml, "clist2");

	if (folder_selected == 0)
		gtk_clist_select_row(GTK_CLIST(tree), folder_selected + 1, 0);
	walk_drop_files(data);
}

void walk_drop_files(GtkSelectionData * data)
{
        GList *files, *li;
        GList *files_to_upload;

        walk_rio_in_use(TRUE);

        files_to_upload = g_list_alloc();
        files = gnome_uri_list_extract_filenames(data->data);

        for (li = files; li; li = g_list_next(li)) {
                const gchar *mimetype;
                gchar *filename = li->data;

                mimetype = gnome_mime_type_of_file(filename);
                if (!mimetype)
                        continue;

                /* if the file is an mp3, add it to of files to upload */
                if (!strcmp("audio/x-mp3", mimetype)) {
                        g_list_append(files_to_upload, g_strdup(filename));
                        continue;
                }

                /* if it's a playlist, add the files from inside the file */
                if (!strcmp("audio/x-mpegurl", mimetype)) {
                        walk_process_m3u(files_to_upload, filename);
                        continue;
                }

                /* if it's a folder, add the mp3s from inside the folder */
                /* gnome uses special/directory as the mimetype */
                if (!strcmp("special/directory", mimetype)) {
                        walk_process_folder(files_to_upload, filename);
                        continue;
                }

                /* if it's something else, ignore it */
        }

        /* First element always NULL, so we start with "next" item */
        if (files_to_upload->next)
                walk_upload(files_to_upload->next);
        g_list_free(files_to_upload);

        walk_rio_in_use(FALSE);
}

void walk_upload(GList * files)
{
        GList *li;
        gint folder;
        gint err;

        walk_which_mem();
        folder = walk_which_folder();

        for (li = files; li; li = g_list_next(li)) {
		gchar *file;

		file = (gchar *) (li->data);
                if (file == NULL)
                        continue;
                walk_long_op();
                err = rio_add_song(rio_hw, folder, file);
                E(err);
                if (err != RIO_SUCCESS) break;
        }

        walk_rescan();
}

void walk_process_m3u(GList * list, gchar * filename)
{
        FILE *input;

        input = fopen(filename, "r");

        if (input == NULL)
                return;

        while (!feof(input)) {
                gchar *foo;
                gchar *line = NULL;

                foo = fgets(line, PATH_MAX_LENGTH, input);
                if (foo != NULL) {
                        const gchar *mimetype;

                        /* removes the trailing new line character */
                        line = g_strchomp(line);
                        mimetype = gnome_mime_type_of_file(line);
                        if (!strcmp("audio/x-mp3", mimetype)) {
                                g_list_append(list, g_strdup(line));
                                continue;
                        }
                }
        }
        fclose(input);
}

void walk_process_folder(GList * list, gchar * filename)
{
        DIR *directory;
        struct dirent *files;

        directory = opendir(filename);
        if (directory == NULL)
                return;

        while ((files = readdir(directory))) {
                const gchar *mimetype;
                gchar *line;

                line = g_strdup_printf("%s/%s", filename, files->d_name);
                mimetype = gnome_mime_type_of_file(line);
                if (!strcmp("audio/x-mp3", mimetype)) {
                        g_list_append(list, g_strdup(line));
                        continue;
                }
        }
        closedir(directory);
}

