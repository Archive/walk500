

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include "rio500.h"

GladeXML *xml = NULL;
GtkWidget *mainwindow;
GtkWidget *about;
gboolean init_finish;

int main(int argc, char *argv[])
{
	init_finish = FALSE;

#ifdef ENABLE_NLS
	bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain(PACKAGE);
#endif

	gnome_init("walk500", VERSION, argc, argv);
	glade_gnome_init();

	/* load the glade file */
	{
		gchar *glade_path;

		glade_path = g_strconcat(W500_DATA, "walk500.glade", NULL);

		xml = glade_xml_new (glade_path, NULL);

		if (xml == NULL)
		{
			walk_io_error(WALK500_GLADE);
			exit(1);
		}
		g_free(glade_path);
	}
	/* connect the signals to the interface */
	glade_xml_signal_autoconnect(xml);
	mainwindow = glade_xml_get_widget(xml, "mainwindow");
	gtk_widget_show(mainwindow);

	walk_init();
	/* Initialization work-around */
	init_finish = TRUE;
	walk_rescan();

	gtk_main();
	return 0;
}
