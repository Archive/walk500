#include <gnome.h>


gboolean
on_mainwindow_destroy_event(GtkWidget * widget,
			    GdkEvent * event, gpointer user_data);

gboolean
on_mainwindow_delete_event(GtkWidget * widget,
			   GdkEvent * event, gpointer user_data);

void
on_new_directory1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_add_song1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_exit1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_refresh1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_format_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_delete_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_move_up_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_move_down_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_memory1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_flash_card1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_preferences1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_properties1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_about1_activate(GtkMenuItem * menuitem, gpointer user_data);

void on_button_newdir_clicked(GtkButton * button, gpointer user_data);

void on_button_refresh_clicked(GtkButton * button, gpointer user_data);

void on_button_format_clicked(GtkButton * button, gpointer user_data);

void on_button_delete_clicked(GtkButton * button, gpointer user_data);

void on_button_up_clicked(GtkButton * button, gpointer user_data);

void on_button_down_clicked(GtkButton * button, gpointer user_data);

void
on_clist1_row_move(GtkCList * clist,
		   gint arg1, gint arg2, gpointer user_data);

void
on_clist1_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_unselect_row(GtkCList * clist,
		       gint row,
		       gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_drag_data_received(GtkWidget * widget,
			     GdkDragContext * drag_context,
			     gint x,
			     gint y,
			     GtkSelectionData * data,
			     guint info, guint time, gpointer user_data);

void on_button_newfile_clicked(GtkButton * button, gpointer user_data);

void on_button_removefile_clicked(GtkButton * button, gpointer user_data);

void on_delete_folder_activate(GtkMenuItem * menuitem, gpointer user_data);

void
on_button_delete_folder_clicked(GtkButton * button, gpointer user_data);

void
on_prefs_apply(GnomePropertyBox * gnomepropertybox,
		      gint arg1, gpointer user_data);

void
on_prefs_help(GnomePropertyBox * gnomepropertybox,
		     gint arg1, gpointer user_data);

void
on_clist1_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_unselect_row(GtkCList * clist,
		       gint row,
		       gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_drag_data_received(GtkWidget * widget,
			     GdkDragContext * drag_context,
			     gint x,
			     gint y,
			     GtkSelectionData * data,
			     guint info, guint time, gpointer user_data);

void
on_clist1_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_unselect_row(GtkCList * clist,
		       gint row,
		       gint column, GdkEvent * event, gpointer user_data);

void
on_clist1_drag_data_received(GtkWidget * widget,
			     GdkDragContext * drag_context,
			     gint x,
			     gint y,
			     GtkSelectionData * data,
			     guint info, guint time, gpointer user_data);

void
on_clist2_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data);

void
on_clist2_drag_data_received(GtkWidget * widget,
			     GdkDragContext * drag_context,
			     gint x,
			     gint y,
			     GtkSelectionData * data,
			     guint info, guint time, gpointer user_data);

void
on_rename_song1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_rename_folder1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_get_song1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_animation1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_create_animation1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clist1_row_move                     (GtkCList        *clist,
                                        gint             arg1,
                                        gint             arg2,
                                        gpointer         user_data);

void
on_move_song_up1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_move_song_down1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_pixmap1_drag_data_received          (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        gint             x,
                                        gint             y,
                                        GtkSelectionData *data,
                                        guint            info,
                                        guint            time,
                                        gpointer         user_data);

void
on_canvas1_drag_data_get               (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        GtkSelectionData *data,
                                        guint            info,
                                        guint            time,
                                        gpointer         user_data);

void
on_canvas1_drag_data_received          (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        gint             x,
                                        gint             y,
                                        GtkSelectionData *data,
                                        guint            info,
                                        guint            time,
                                        gpointer         user_data);

void
on_radiobutton3_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

gboolean
on_druidpagestandard3_next             (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

gboolean
on_druidpagestart2_next                (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_druidpagestandard3_prepare          (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_druidpagefinish2_prepare            (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_combo_entry4_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_druidpagefinish2_finish             (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_druidpagestandard4_prepare          (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_druidpagestandard6_prepare          (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_druidpagefinish3_finish             (GnomeDruidPage  *gnomedruidpage,
                                        gpointer         arg1,
                                        gpointer         user_data);

void
on_entry1_changed                      (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_checkbutton6_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_radiobutton5_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_entry4_changed                      (GtkEditable     *editable,
                                        gpointer         user_data);
void
on_about_close                          (GtkWidget * widget,
                                        GdkEvent * event, gpointer user_data);

void
on_prefs_close                          (GtkWidget * widget,
                                        GdkEvent * event, gpointer user_data);

void
on_props_close				(GtkWidget * widget,
					GdkEvent * event, gpointer user_data);

void
on_entry1_changed                      (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_checkbutton6_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);


void
on_button4_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_button6_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_entry5_changed                      (GtkEditable     *editable,
                                        gpointer         user_data);
void
on_checkbutton_toggled		       (GtkToggleButton *togglebutton,
					gpointer user_data);

void
on_pause_check_toggled		       (GtkToggleButton *togglebutton,
					gpointer user_data);

void
on_remove_animation1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_last_pause_toggle_toggled	       (GtkToggleButton *togglebutton,
					gpointer user_data);

void
on_input_entry_changed                 (GtkEditable     *editable,
		                        gpointer         user_data);

void
on_animbmp_ok_button_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_animbmp_cancel_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_dialogbmp_close                     (GtkWidget * widget,
                                        GdkEvent * event, gpointer user_data);

