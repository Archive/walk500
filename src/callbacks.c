#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"

#include "rio500.h"

//extern GtkWidget *mainwindow, *about;
extern gboolean init_finish;
extern GladeXML *xml;

gboolean
on_mainwindow_destroy_event(GtkWidget * widget,
			    GdkEvent * event, gpointer user_data)
{
	walk_close();
	return TRUE;
}


gboolean
on_mainwindow_delete_event(GtkWidget * widget,
			   GdkEvent * event, gpointer user_data)
{
	walk_close();
	return TRUE;
}


void on_new_directory1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_add_folder();
}


void on_add_song1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_add_song_dialog();
}


void on_exit1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_close();
}


void on_refresh1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_rescan();
}


void on_format_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_format();
}


void on_delete_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_delete_song();
}


void on_preferences1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_prefs_activate();
}

void on_properties1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	GtkWidget *props, *mainwindow;

	props = glade_xml_get_widget(xml, "props");
        mainwindow = glade_xml_get_widget(xml, "mainwindow");
        gnome_dialog_set_parent(GNOME_DIALOG(props), GTK_WINDOW(mainwindow));
        gtk_widget_show(props);
}

void on_about1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	GtkWidget *about, *mainwindow;

	about = glade_xml_get_widget(xml, "about");
	mainwindow = glade_xml_get_widget(xml, "mainwindow");
	gnome_dialog_set_parent(GNOME_DIALOG(about), GTK_WINDOW(mainwindow));
	gtk_widget_show(about);
}


void on_button_newdir_clicked(GtkButton * button, gpointer user_data)
{
	walk_add_folder();
}


void on_button_refresh_clicked(GtkButton * button, gpointer user_data)
{
	walk_rescan();
}


void on_button_format_clicked(GtkButton * button, gpointer user_data)
{
	walk_format();
}


void on_button_delete_clicked(GtkButton * button, gpointer user_data)
{
	walk_delete_song();
}


void
on_clist1_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data)
{
	walk_select_song(row);
}


void
on_clist1_unselect_row(GtkCList * clist,
		       gint row,
		       gint column, GdkEvent * event, gpointer user_data)
{
	walk_select_song(-1);
}


void
on_clist1_drag_data_received(GtkWidget * widget,
			     GdkDragContext * drag_context,
			     gint x,
			     gint y,
			     GtkSelectionData * data,
			     guint info, guint time, gpointer user_data)
{
	gtk_drag_finish(drag_context, TRUE, FALSE, time);
	walk_drop_files(data);
}


void on_button_newfile_clicked(GtkButton * button, gpointer user_data)
{
	walk_add_song_dialog();
}


void on_delete_folder_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	walk_delete_folder();
}


void
on_button_delete_folder_clicked(GtkButton * button, gpointer user_data)
{
	walk_delete_folder();
}


void
on_prefs_apply(GnomePropertyBox * gnomepropertybox,
		      gint arg1, gpointer user_data)
{
	walk_prefs_apply(arg1);
}


void
on_prefs_help(GnomePropertyBox * gnomepropertybox,
		     gint arg1, gpointer user_data)
{

}


void
on_rename_song1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	walk_rename_song();
}


void
on_rename_folder1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	walk_rename_folder();
}


void
on_get_song1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	walk_get_song_dialog();
}


void
on_animation1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *dialogani, *mainwindow;

        dialogani = glade_xml_get_widget(xml, "dialoganim");
        mainwindow = glade_xml_get_widget(xml, "mainwindow");
        gnome_dialog_set_parent(GNOME_DIALOG(dialogani),
			GTK_WINDOW(mainwindow));
        gtk_widget_show(dialogani);
}


void
on_create_animation1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *dialogbmp, *mainwindow;

        dialogbmp = glade_xml_get_widget(xml, "dialogbmp");
        mainwindow = glade_xml_get_widget(xml, "mainwindow");
        gnome_dialog_set_parent(GNOME_DIALOG(dialogbmp),
                        GTK_WINDOW(mainwindow));
        gtk_widget_show(dialogbmp);
}


void
on_move_song_up1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	walk_swap_songs(-1);
}


void
on_move_song_down1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	walk_swap_songs(1);
}


void
on_canvas1_drag_data_received          (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        gint             x,
                                        gint             y,
                                        GtkSelectionData *data,
                                        guint            info,
                                        guint            time,
                                        gpointer         user_data)
{
	gtk_drag_finish(drag_context, TRUE, FALSE, time);
	walk_drop_onpix(data);
}


void
on_about_close				(GtkWidget * widget,
					GdkEvent * event, gpointer user_data)
{
	GtkWidget *about;

	about = glade_xml_get_widget(xml, "about");
	gtk_widget_hide(about);
}

void
on_prefs_close                          (GtkWidget * widget,
                                        GdkEvent * event, gpointer user_data)
{
	GtkWidget *prefs;

	prefs = glade_xml_get_widget(xml, "prefs");
	gtk_widget_hide(prefs);
}

void
on_props_close				(GtkWidget * widget,
					GdkEvent * event, gpointer user_data)
{
	GtkWidget *props;

	props = glade_xml_get_widget(xml, "props");
	gtk_widget_hide(props);
}

void
on_button4_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
        walk_dialogani_apply();
}

void
on_button6_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
        walk_dialogani_cancel();
}

void
on_entry5_changed                      (GtkEditable     *editable,
                                        gpointer         user_data)
{
        walk_dialogani_ckinput(GTK_WIDGET(editable));
}

void
on_input_entry_changed                      (GtkEditable     *editable,
	                                     gpointer         user_data)
{
	walk_dialogbmp_ckinput(GTK_WIDGET(editable));
}


void
on_remove_animation1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        walk_ani_remove();
}

void
on_checkbutton_toggled(GtkToggleButton * togglebutton, gpointer user_data)
{
        GtkWidget *prefs;

        prefs = glade_xml_get_widget(xml, "prefs");
	if (GTK_WIDGET_VISIBLE(prefs))
	        gnome_property_box_changed(GNOME_PROPERTY_BOX(prefs));
}

void
on_pause_check_toggled(GtkToggleButton * togglebutton, gpointer user_data)
{
	GtkWidget *tmp_widget = NULL;
	gboolean state;

	state = gtk_toggle_button_get_active(togglebutton);
	tmp_widget = glade_xml_get_widget(xml, "pause_hbox");
	gtk_widget_set_sensitive(tmp_widget, state);
}

void
on_last_pause_toggle_toggled(GtkToggleButton * togglebutton, gpointer user_data)
{
	GtkWidget *tmp_widget = NULL;
	gboolean state;

	state = gtk_toggle_button_get_active(togglebutton);
	tmp_widget = glade_xml_get_widget(xml, "lastpause_hbox");
	gtk_widget_set_sensitive(tmp_widget, state);

	/* Invert the state */
	state = (state == TRUE ? FALSE : TRUE);
	tmp_widget = glade_xml_get_widget(xml, "loop_hbox");
	gtk_widget_set_sensitive(tmp_widget, state);
}

void
on_animbmp_ok_button_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{
	walk_dialogbmp_cancel();
	walk_dialogbmp_apply();
}

void
on_animbmp_cancel_clicked	       (GtkButton       *button,
					gpointer         user_data)
{
	walk_dialogbmp_cancel();
}

void
on_dialogbmp_close                     (GtkWidget * widget,
                                        GdkEvent * event, gpointer user_data)
{
	walk_dialogbmp_cancel();
}

