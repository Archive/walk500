#ifndef __RIO500_H__
#define __RIO500_H__


#include <librio500_api.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "bmp2rioani.h"

#define WALK500_GLADE  -11  /* error while converting bmp file to animation */

/* macro to make the code more readable */
#define E(x) walk_check_error(x)

/* rio500.c */
void walk_rescan(void);
void walk_select_song(gint row);
void walk_update_clist(gint num_folder, gint card);
void walk_update_tree(void);
void walk_update_tree_card(gint card);
void walk_long_op(void);
void walk_close(void);
void walk_init(void);
int walk_card_count(void);
void walk_update_statusbar(int op, char *msg, int percent);
void walk_rio_in_use(gboolean data);
void on_clist2_select_row(GtkCList * clist, gint row, gint column,
			  GdkEvent * event, gpointer user_data);
void walk_update_summary(void);
gint walk_which_mem(void);
gint walk_which_folder(void);
void walk_swap_songs(int direction);
void walk_pixmap_init(void);
GdkPixbuf *walk_pixmap_from_file(gchar * name);
void walk_add_folder_info(gint folder, gint mem);
void walk_add_folder_data(gchar * text, GdkPixmap *pixmap, GdkPixmap *bitmap);
void walk_destroy_info(gpointer data, gpointer user_data);
GdkPixmap *walk_pixmap(GdkPixbuf *pixbuf);
GdkBitmap *walk_bitmap(GdkPixbuf *pixbuf);
void walk_check_error(gint error);

/* fileops.c */
void walk_add_song(GtkWidget * widget);
void walk_get_song(GtkWidget * widget);
void walk_drop_onpix(GtkSelectionData * data);
void walk_drop_files(GtkSelectionData * data);
void walk_upload(GList * files);
void walk_process_m3u(GList * list, gchar * filename);
void walk_process_folder(GList * list, gchar * filename);

/* dialogs.c */
void walk_add_song_dialog(void);
void walk_add_folder(void);
void walk_delete_folder(void);
void walk_delete_song(void);
void walk_format(void);
void walk_init_fail_dialog(void);
void walk_io_error(gint err);
void walk_rename_folder(void);
void walk_rename_song (void);
void walk_get_song_dialog(void);

/* prefs.c */
void walk_prefs_apply(gint page_num);
void walk_load_config(void);
void walk_save_config(void);
void walk_prefs_activate(void);

/* anim.c */
void walk_dialogbmp_ckinput(GtkWidget *entry);
void walk_dialogbmp_apply(void);
void walk_dialogbmp_activate(void);
void walk_dialogbmp_cancel(void);
void walk_dialogbmp_ckpause(GtkWidget *toggle);
void walk_dialogbmp_ckloop(GtkWidget *toggle);
void walk_dialogbmp_create(void);

void walk_dialogani_activate(void);
void walk_dialogani_cancel(void);
void walk_dialogani_ckinput(GtkWidget *entry);
void walk_dialogani_create(void);
void walk_dialogani_cancel(void);
void walk_dialogani_apply(void);

void walk_ani_remove(void);

/* folder information for clist2 */
typedef struct {
	gint folder_num;	/* -1 if the "tree" item is not a folder */
	gint memtype;		/* 0 for internal, 1 for flash */
	gint callback_id;	/* Callback ID for the specified folder */
} FolderInfo;

/* It's from librio500.c, but not included in the librio500.h  */
unsigned long query_card_count(int fd);

/* dialog windows */
extern GtkWidget *prefs;
extern GtkWidget *about;

/* Rio500 stuff */
extern Rio500 *rio_hw;
extern GList *content, *folder_list;
extern gint folder_num;

/* Configuration values */
extern gboolean confirm_format;
extern gboolean confirm_del;
extern gboolean confirm_del_folder;
extern gboolean confirm_del_empty_folder;

#endif				/* __RIO500_H__ */
