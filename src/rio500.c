

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "callbacks.h"
#include "rio500.h"

static gchar  *walk_human_size(gint size);

extern GtkWidget *mainwindow;
extern gboolean init_finish;
extern GladeXML *xml;

gint song_selected = -1;
GList *tree_list;		/* All the infos of CList2 items */
gint folder_selected;		/* Position of the selected row in clist2 */
guint callback_id;		/* callback for clist2's select-row signal */
gboolean rio_in_use;

/* Pixmaps */
//GdkPixbuf *folder_pix, *rio500_pix, *flash_pix, *song_pix;
GdkPixmap *folder_pixmap, *folder_bitmap;
GdkPixmap *rio500_pixmap, *rio500_bitmap;
GdkPixmap *flash_pixmap, *flash_bitmap;
GdkPixmap *song_pixmap, *song_bitmap;

/* Rio500 stuff */
Rio500 *rio_hw;
GList *int_content, *ext_content;
gulong int_mem_total, ext_mem_total;
gulong int_mem_free, ext_mem_free;
gint num_cards;

const GtkTargetEntry target_table[] = {
	{"text/plain", 0, 0}
};

void walk_rescan(void)
{
	GtkWidget *tmp_widget;
	gint old_folder;

	walk_rio_in_use(TRUE);
	old_folder = folder_selected;

	rio_set_card(rio_hw, 0);

	if (int_content)
		rio_destroy_content(int_content);
	int_content = rio_get_content(rio_hw);
	int_mem_free = rio_memory_left(rio_hw);

	if (num_cards >= 2) {
		rio_set_card(rio_hw, 1);
		if (ext_content)
			rio_destroy_content(ext_content);
		ext_content = rio_get_content(rio_hw);
		ext_mem_free = rio_memory_left(rio_hw);
		rio_set_card(rio_hw, 0);
	} else {
		ext_content = NULL;
	}

	walk_long_op();

	walk_update_tree();

	walk_update_summary();

	/* Here we select the previously selected folder (kinda) or 
	 * or the RAM header if no folder exist */

	tmp_widget = glade_xml_get_widget(xml, "clist2");
	{
		GList *tmp_list;

		tmp_list = g_list_nth(tree_list, old_folder);
		if (tmp_list) {
			gtk_clist_select_row(GTK_CLIST(tmp_widget),
					     old_folder, 0);
		} else {
			gtk_clist_select_row(GTK_CLIST(tmp_widget), 0, 0);
		}
	}

	walk_rio_in_use(FALSE);
}

void walk_update_tree(void)
{
	GtkWidget *clist2;

	clist2 = glade_xml_get_widget(xml, "clist2");
	gtk_signal_disconnect(GTK_OBJECT(clist2), callback_id);

	gtk_clist_freeze(GTK_CLIST(clist2));
	gtk_clist_clear(GTK_CLIST(clist2));

	/* Destroy the content of the tree_list if it exists */
	if (tree_list) {
		g_list_foreach(tree_list, walk_destroy_info, NULL);
		g_list_free(tree_list);
		tree_list = NULL;
	}

	/* Internal RAM Header */

	walk_add_folder_info(-1, 0);
	walk_add_folder_data(_("Internal RAM"), rio500_pixmap, rio500_bitmap);

	/* Internal RAM Content */

	walk_update_tree_card(0);

	if (num_cards == 2) {
		/* External RAM Header */

		walk_add_folder_info(-1, 1);
		walk_add_folder_data(_("Flash Card"), flash_pixmap,
				flash_bitmap);

		/* External RAM Content */

		walk_update_tree_card(1);
	}

	callback_id = gtk_signal_connect(GTK_OBJECT(clist2), "select-row",
					 on_clist2_select_row, NULL);
	gtk_clist_thaw(GTK_CLIST(clist2));
}

void walk_update_tree_card(gint card)
{
	GtkWidget *clist2;
	GList *folder;
	GList *content;
	RioFolderEntry *folder_entry;

	clist2 = glade_xml_get_widget(xml, "clist2");

	if (card == 0) {
		content = int_content;
	} else {
		content = ext_content;
	}

	for (folder = content; folder; folder = folder->next) {
		folder_entry = (RioFolderEntry *) folder->data;
		if (folder_entry) {
			walk_add_folder_info(folder_entry->folder_num,
					     card);
			walk_add_folder_data(folder_entry->name,
					     folder_pixmap, folder_bitmap);

		}
	}
}

void walk_update_clist(gint card, gint num_folder)
{
	GtkWidget *clist;
	GList *content, *song;
	RioSongEntry *song_entry;
	RioFolderEntry *folder_entry;
	gchar *tmp[2];
	gint i;

	clist = glade_xml_get_widget(xml, "clist1");
	gtk_clist_freeze(GTK_CLIST(clist));
	gtk_drag_dest_unset(clist);
	gtk_clist_clear(GTK_CLIST(clist));

	if (card == 0) {
		content = int_content;
	} else {
		content = ext_content;
	}

	if ((content == NULL) || (g_list_length(content) == 0)
			|| (num_folder == -1)) {
		gtk_clist_clear(GTK_CLIST(clist));
		gtk_clist_thaw(GTK_CLIST(clist));
		return;
	}
	if (num_folder >= g_list_length(content)) {
		num_folder = 0;
		walk_update_tree();
	}

	gtk_drag_dest_set(clist,
			  GTK_DEST_DEFAULT_ALL,
			  target_table, 1, GDK_ACTION_COPY);

	folder_entry =
	    (RioFolderEntry *) (g_list_nth_data(content, num_folder));
	if (folder_entry->songs == NULL) {
		gtk_clist_clear(GTK_CLIST(clist));
		gtk_clist_thaw(GTK_CLIST(clist));
		return;
	}

	for (song = folder_entry->songs; song; song = song->next) {
		song_entry = (RioSongEntry *) song->data;
		if (song_entry) {
			tmp[0] = g_strdup(song_entry->name);
			tmp[1] = walk_human_size(song_entry->size);

			i = gtk_clist_append(GTK_CLIST(clist), tmp);
			if (song != NULL) {
				if (song_pixmap != NULL)
				{
					gtk_clist_set_pixtext(GTK_CLIST(clist),
						      i, 0,
						      tmp[0], 8,
						      song_pixmap,
						      song_bitmap);
				} else {
					gtk_clist_set_text(GTK_CLIST(clist),
							i, 0, tmp[0]);
				}
			} else {
				gtk_clist_set_text(GTK_CLIST(clist), i, 0,
						   tmp[0]);
			}
			/* it doesn't seem to copy the data... */
			gtk_clist_set_row_data(GTK_CLIST(clist), i,
					       g_strdup(tmp[0]));
			for (i = 0; i < 2; i++) g_free(tmp[i]);
		}
	}

	gtk_clist_thaw(GTK_CLIST(clist));
	walk_select_song(-1);
}

void walk_select_song(gint row)
{
	GtkWidget *button_tmp;
	gboolean sensitive;

	if (row == -1) {
		sensitive = FALSE;
	} else {
		sensitive = TRUE;
	}

	song_selected = row;

	button_tmp =
	    glade_xml_get_widget(xml, "button_delete");
	gtk_widget_set_sensitive(button_tmp, sensitive);

	button_tmp = glade_xml_get_widget(xml, "delete");
	gtk_widget_set_sensitive(button_tmp, sensitive);

	button_tmp = glade_xml_get_widget(xml, "rename_song1");
	gtk_widget_set_sensitive(button_tmp, sensitive);

	button_tmp = glade_xml_get_widget(xml, "get_song1");
	gtk_widget_set_sensitive(button_tmp, sensitive);

	button_tmp = glade_xml_get_widget(xml,
			"move_song_up1");
	if (song_selected == 0)
	{
		gtk_widget_set_sensitive(button_tmp, FALSE);
		button_tmp = glade_xml_get_widget(xml,
				"move_song_down1");
		gtk_widget_set_sensitive(button_tmp, sensitive);
	} else {
		gint num_songs;

		gtk_widget_set_sensitive(button_tmp, sensitive);
		button_tmp = glade_xml_get_widget(xml, "clist1");
		num_songs = GTK_CLIST(button_tmp)->rows - 1;

		button_tmp = glade_xml_get_widget(xml,
				"move_song_down1");
		if (song_selected == num_songs)
		{
			gtk_widget_set_sensitive(button_tmp, FALSE);
		} else {
			gtk_widget_set_sensitive(button_tmp, sensitive);
		}
	}
}

void walk_update_statusbar(int op, char *msg, int percent)
{
	GtkWidget *statusbar;
	GtkProgress *progress;

	statusbar = glade_xml_get_widget(xml, "appbar");
	progress = gnome_appbar_get_progress(GNOME_APPBAR(statusbar));

	/* Set the percentage of completion of the current action */
	gtk_progress_set_value(GTK_PROGRESS(progress), percent);

	/* Set the name of the current action */
	gnome_appbar_set_status(GNOME_APPBAR(statusbar), msg);

	walk_long_op();
}

void walk_long_op(void)
{
	while (gtk_events_pending())
		gtk_main_iteration();
}

void walk_close(void)
{
	if ((init_finish == TRUE) && (rio_in_use == FALSE))
	{
		if (int_content) rio_destroy_content(int_content);
		if (ext_content) rio_destroy_content(ext_content);

		if (folder_pixmap) gdk_pixmap_unref(folder_pixmap);
		if (folder_bitmap) gdk_pixmap_unref(folder_bitmap);
		if (rio500_pixmap) gdk_pixmap_unref(rio500_pixmap);
		if (rio500_bitmap) gdk_pixmap_unref(rio500_bitmap);
		if (flash_pixmap) gdk_pixmap_unref(flash_pixmap);
		if (flash_bitmap) gdk_pixmap_unref(flash_bitmap);
		if (song_pixmap) gdk_pixmap_unref(song_pixmap);
		if (song_bitmap) gdk_pixmap_unref(song_bitmap);

		walk_save_config();
		rio_delete(rio_hw);
		gtk_exit(0);
	}
	return;
}

void walk_init(void)
{
	GdkPixbuf *tmp_pixmap;
	GtkWidget *tmp_widget;

	/* Set up the window icon and in the summary*/
	tmp_pixmap = walk_pixmap_from_file("gnome-walk500.png");

	if (tmp_pixmap != NULL) {
		GnomeCanvasItem *item;
		double x, y;		/* size of the pixbuf */

		gdk_window_set_icon(mainwindow->window, NULL,
				    walk_pixmap(tmp_pixmap),
				    walk_bitmap(tmp_pixmap));

		tmp_widget = glade_xml_get_widget(xml, "canvas1");
		x = gdk_pixbuf_get_width(tmp_pixmap);
		y = gdk_pixbuf_get_height(tmp_pixmap);
		gnome_canvas_set_scroll_region(GNOME_CANVAS(tmp_widget),
				0.0, 0.0, x, y);
		item = gnome_canvas_item_new
			(gnome_canvas_root(GNOME_CANVAS(tmp_widget)),
			gnome_canvas_pixbuf_get_type(),
			"pixbuf", tmp_pixmap, NULL);
		gnome_canvas_item_move (item, 0, 0);
		gdk_pixbuf_unref(tmp_pixmap);
	}

	walk_pixmap_init();

	rio_hw = rio_new();
	while (rio_check(rio_hw) == FALSE) {
		walk_init_fail_dialog();
		rio_hw = rio_init(rio_hw);
	}

	rio_set_report_func(rio_hw, walk_update_statusbar);

	rio_set_card(rio_hw, 0);
	num_cards = walk_card_count();

	if (num_cards >= 2) {
		rio_set_card(rio_hw, 1);
		ext_mem_total = rio_get_mem_total(rio_hw);
		rio_set_card(rio_hw, 0);
	} else {
		ext_mem_total = 0;
	}

	int_mem_total = rio_get_mem_total(rio_hw);

	tmp_widget = glade_xml_get_widget(xml, "progressbar1");
	gtk_progress_configure(GTK_PROGRESS(tmp_widget), 0, 0,
			       int_mem_total / (1024 * 1024));

	/* GTK is a bit greedy on the height of clist items */
	tmp_widget = glade_xml_get_widget(xml, "clist1");
	gtk_clist_set_row_height(GTK_CLIST(tmp_widget), 16);

	tmp_widget = glade_xml_get_widget(xml, "clist2");
	gtk_clist_set_row_height(GTK_CLIST(tmp_widget), 16);

	callback_id =
	    gtk_signal_connect(GTK_OBJECT(tmp_widget), "select-row",
			       on_clist2_select_row, NULL);

	tmp_widget = glade_xml_get_widget(xml, "canvas1");
	gtk_drag_dest_set(tmp_widget,
			GTK_DEST_DEFAULT_ALL,
			target_table, 1, GDK_ACTION_COPY);

	walk_load_config();

	return;
}

int walk_card_count(void)
{
	return (int) query_card_count(rio_hw->rio_dev);
}

void walk_rio_in_use(gboolean data)
{
	GtkWidget *tmp_widget;
	gboolean sensitive;

	rio_in_use = data;

	if (data == TRUE) {
		sensitive = FALSE;
	} else {
		sensitive = TRUE;
	}

	/* The menubar, for some reason, dock1 in the sources and
	 * dockitem1 in Glade... */
	tmp_widget = glade_xml_get_widget(xml, "dock1");
	gtk_widget_set_sensitive(GTK_WIDGET(tmp_widget), sensitive);
	/* The toolbar */
	tmp_widget = glade_xml_get_widget(xml, "toolbar1");
	gtk_widget_set_sensitive(GTK_WIDGET(tmp_widget), sensitive);
	/* The Clists */
	tmp_widget =
	    glade_xml_get_widget(xml, "hpaned1");
	gtk_widget_set_sensitive(GTK_WIDGET(tmp_widget), sensitive);

	walk_long_op();
}

void
on_clist2_select_row(GtkCList * clist,
		     gint row,
		     gint column, GdkEvent * event, gpointer user_data)
{
	GtkWidget *tmp_widget;
	FolderInfo *info;
	gboolean sensitive;

	if (init_finish == FALSE)
		return;

	folder_selected = row;

	info = g_list_nth_data(tree_list, row);

	if (info->folder_num == -1) {
		sensitive = FALSE;
	} else {
		sensitive = TRUE;
	}
	walk_update_clist(info->memtype, info->folder_num);

	tmp_widget = glade_xml_get_widget(xml, "progressbar1");
	if (info->memtype == 0) {
		gtk_progress_configure(GTK_PROGRESS(tmp_widget),
				       int_mem_free / (1024 * 1024), 0,
				       int_mem_total / (1024 * 1024));
	} else {
		gtk_progress_configure(GTK_PROGRESS(tmp_widget),
				       ext_mem_free / (1024 * 1024), 0,
				       ext_mem_total / (1024 * 1024));
	}

	tmp_widget =
	    glade_xml_get_widget(xml, "button_delete_folder");
	gtk_widget_set_sensitive(tmp_widget, sensitive);
	tmp_widget =
	    glade_xml_get_widget(xml, "delete_folder");
	gtk_widget_set_sensitive(tmp_widget, sensitive);
	tmp_widget =
	    glade_xml_get_widget(xml, "button_newfile");
	gtk_widget_set_sensitive(tmp_widget, sensitive);
	tmp_widget = glade_xml_get_widget(xml, "add_song1");
	gtk_widget_set_sensitive(tmp_widget, sensitive);
	tmp_widget =
		glade_xml_get_widget(xml, "rename_folder1");
	gtk_widget_set_sensitive(tmp_widget, sensitive);

	/* And the song manipulation buttons as well should be disabled
	 * ... if we disable the folder manipulation as well */
	if (sensitive == FALSE)
	{
	        tmp_widget =
  	          glade_xml_get_widget(xml, "button_delete");
	        gtk_widget_set_sensitive(tmp_widget, sensitive);

		tmp_widget = glade_xml_get_widget(xml, "delete");
	        gtk_widget_set_sensitive(tmp_widget, sensitive);

	        tmp_widget =
			glade_xml_get_widget(xml, "rename_song1");
	        gtk_widget_set_sensitive(tmp_widget, sensitive);

	        tmp_widget = glade_xml_get_widget(xml, "get_song1");
	        gtk_widget_set_sensitive(tmp_widget, sensitive);
	}
}

void walk_update_summary(void)
{
	GtkWidget *tmp_widget;
	gint revision;
	gchar *tmp;

	/* Firmware version */
	tmp_widget = glade_xml_get_widget(xml, "label26");

	revision = query_firmware_rev(rio_hw->rio_dev);
	tmp = g_strdup_printf("%d.%02x", revision >> 8, (revision & 0xff));
	gtk_label_set_text(GTK_LABEL(tmp_widget), tmp);

	g_free(tmp);

	/* Internal Memory */
	tmp_widget = glade_xml_get_widget(xml, "label28");
	tmp = g_strdup_printf(_("%lu MB out of %lu MB free"),
			      int_mem_free / (1024 * 1024),
			      int_mem_total / (1024 * 1024));
	gtk_label_set_text(GTK_LABEL(tmp_widget), tmp);

	g_free(tmp);

	/* Flash Memory */
	tmp_widget = glade_xml_get_widget(xml, "label30");

	if (num_cards >= 2) {
		tmp = g_strdup_printf(_("%lu MB out of %lu MB free"),
				      ext_mem_free / (1024 * 1024),
				      ext_mem_total / (1024 * 1024));
		gtk_label_set_text(GTK_LABEL(tmp_widget), tmp);

		g_free(tmp);
	} else {
		gtk_label_set_text(GTK_LABEL(tmp_widget), _("N/A"));
	}
}

gint walk_which_mem(void)
{
	FolderInfo *info;

	info = g_list_nth_data(tree_list, folder_selected);
	if (info->memtype == 0) {
		rio_set_card(rio_hw, 0);
		return 0;
	} else {
		rio_set_card(rio_hw, 1);
		return 1;
	}
}

gint walk_which_folder(void)
{
	FolderInfo *info;
	gint folder;

	info = g_list_nth_data(tree_list, folder_selected);
	folder = info->folder_num;

	return folder;
}

void walk_add_folder_info(gint folder, gint mem)
{
	FolderInfo *info;

	info = g_new(FolderInfo, 1);
	info->folder_num = folder;
	info->memtype = mem;
	tree_list = g_list_append(tree_list, (gpointer) info);
}

void walk_destroy_info(gpointer data, gpointer user_data)
{
	g_free(data);
}

void walk_add_folder_data(gchar * text, GdkPixmap *pixmap, GdkPixmap *bitmap)
{
	GtkWidget *clist2;
	gchar *tmp[1];
	gint i;

	clist2 = glade_xml_get_widget(xml, "clist2");

	tmp[0] = text;
	i = gtk_clist_append(GTK_CLIST(clist2), tmp);

	if ((pixmap != NULL) && (bitmap != NULL)) {
		gtk_clist_set_pixtext(GTK_CLIST(clist2), i, 0, tmp[0], 8,
				      pixmap, bitmap);
	} else {
		gtk_clist_set_text(GTK_CLIST(clist2), i, 0, tmp[0]);
	}
	gtk_clist_set_row_data(GTK_CLIST(clist2), i, g_strdup(tmp[0]));
}

void walk_swap_songs(gint direction)
{
	gint other_song, folder;

	other_song = song_selected + direction;
	folder = walk_which_folder();
	walk_which_mem();
//FIXME
	g_print("direction: %d select: %d other: %d\n", direction,
			song_selected, other_song);

	walk_rio_in_use(TRUE);
	E(rio_swap_songs(rio_hw, folder, song_selected, other_song));
	walk_rescan();
	walk_rio_in_use(FALSE);
}

void walk_pixmap_init(void)
{
	GdkPixbuf *folder_pix, *rio500_pix, *flash_pix, *song_pix;

	folder_pix = walk_pixmap_from_file("directory.xpm");
	folder_pixmap = walk_pixmap(folder_pix);
	folder_bitmap = walk_bitmap(folder_pix);
	gdk_pixbuf_unref(folder_pix);
	
	rio500_pix = walk_pixmap_from_file("internal.png");
	rio500_pixmap = walk_pixmap(rio500_pix);
	rio500_bitmap = walk_bitmap(rio500_pix);
	gdk_pixbuf_unref(rio500_pix);
	
	flash_pix = walk_pixmap_from_file("flash.png");
	flash_pixmap = walk_pixmap(flash_pix);
	flash_bitmap = walk_bitmap(flash_pix);
	gdk_pixbuf_unref(flash_pix);
	
	song_pix = walk_pixmap_from_file("mp3.png");
	song_pixmap = walk_pixmap(song_pix);
	song_bitmap = walk_bitmap(song_pix);
	gdk_pixbuf_unref(song_pix);
}

GdkPixbuf *walk_pixmap_from_file(gchar * name)
{
	GdkPixbuf *pixmap;
	gchar *realname;

	g_return_val_if_fail(name != NULL, NULL);

	realname = g_strconcat(W500_IMAGES, name, NULL);

	if (realname != NULL) {
		pixmap = gdk_pixbuf_new_from_file(realname);
		g_free(realname);
	} else {
		pixmap = NULL;
	}
	return pixmap;
}

GdkPixmap *walk_pixmap(GdkPixbuf *pixbuf)
{
	GdkPixmap *pixmap_return;

	gdk_pixbuf_render_pixmap_and_mask(pixbuf, &pixmap_return,
			NULL, 50);
	return pixmap_return;
}

GdkBitmap *walk_bitmap(GdkPixbuf *pixbuf)
{
	GdkBitmap *mask_return;

	gdk_pixbuf_render_pixmap_and_mask(pixbuf, NULL,
			&mask_return, 50);
	return mask_return;
}

void walk_check_error(gint error)
{
	if (error != RIO_SUCCESS)
		walk_io_error(error);
	return;
}

/* From e-cell-size.c in GAL */
static gchar  *walk_human_size(gint size)
{
	gfloat fsize;

	if (size < 1024) {
		return g_strdup_printf (_("%d bytes"), size);
	} else {
		fsize = ((gfloat) size) / 1024.0;
		if (fsize < 1024.0) {
			return g_strdup_printf (_("%d K"), (int)fsize);
		} else {
			fsize /= 1024.0;
			return g_strdup_printf (_("%.1f MB"), fsize);
		}
	}
}

